package jp.ksu.tmdlab.mican.insn;

public class IincInsn implements java.io.Serializable{
    private static final long serialVersionUID = 3760382139406068526L;

    private int var;
    private int increment;

    public IincInsn(){
    }

    public IincInsn(int var, int increment){
        setVar(var);
        setIncrement(increment);
    }

    public int getVar(){
        return var;
    }

    public int getIncrement(){
        return increment;
    }

    public void setVar(int var){
        if(var < 1){
            throw new IllegalArgumentException();
        }
        this.var = var;
    }

    public void setIncrement(int increment){
        this.increment = increment;
    }
}
