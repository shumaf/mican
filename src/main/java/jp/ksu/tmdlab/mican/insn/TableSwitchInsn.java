package jp.ksu.tmdlab.mican.insn;

import java.io.Serializable;
import java.util.Arrays;

import org.objectweb.asm.Label;

public class TableSwitchInsn implements Serializable {
    private static final long serialVersionUID = -6726960064933357533L;

    private int min, max;
    private Label dflt;
    private Label[] labels;

    public TableSwitchInsn() {
    }

    public TableSwitchInsn(int min, int max, Label dflt, Label[] labels) {
        setMin(min);
        setMax(max);
        setDefault(dflt);
        setLabels(labels);
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public Label getDefault() {
        return dflt;
    }

    public void setDefault(Label dflt) {
        this.dflt = dflt;
    }

    public Label[] getLabels() {
        return Arrays.copyOf(labels, labels.length);
    }

    public void setLabels(Label[] labels) {
        this.labels = Arrays.copyOf(labels, labels.length);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMin()).append(", ").append(getMax());
        sb.append(", ").append(getDefault()).append(", ");
        sb.append(Arrays.toString(labels));

        return new String(sb);
    }
}
