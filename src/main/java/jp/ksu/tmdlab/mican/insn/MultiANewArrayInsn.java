package jp.ksu.tmdlab.mican.insn;

import java.io.Serializable;

public class MultiANewArrayInsn implements Serializable {
    private static final long serialVersionUID = -8477772290730783333L;

    private String description;
    private int dims;

    public MultiANewArrayInsn(){
    }

    public MultiANewArrayInsn(String description, int dims){
        setDescription(description);
        setDims(dims);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDims() {
        return dims;
    }

    public void setDims(int dims) {
        this.dims = dims;
    }

    @Override
    public String toString(){
        return getDescription() +", " + getDims();
    }
}
