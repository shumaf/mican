package jp.ksu.tmdlab.mican.insn;

public class InvokeMethodInsn implements java.io.Serializable{
    private static final long serialVersionUID = -7508404394931343139L;

    private String owner;
    private String name;
    private String description;

    public InvokeMethodInsn(){
    }

    public InvokeMethodInsn(String owner, String name, String description){
        setOwner(owner);
        setName(name);
        setDescription(description);
    }

    public String getName(){
        return name;
    }

    public String getOwner(){
        return owner;
    }

    public String getDescription(){
        return description;
    }

    public void setName(String name){
        if(name == null){
            throw new NullPointerException();
        }
        this.name = name;
    }

    public void setOwner(String owner){
        if(owner == null){
            throw new NullPointerException();
        }
        this.owner = owner;
    }

    public void setDescription(String description){
        if(description == null){
            throw new NullPointerException();
        }
        this.description = description;
    }

    @Override
    public String toString(){
        return getOwner() + ", " + getName() + ", " + getDescription();
    }
}
