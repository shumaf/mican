package jp.ksu.tmdlab.mican.insn;

public class InvokeDynamicInsn implements java.io.Serializable{
    private static final long serialVersionUID = 187953580090121030L;

    private String name;
    private String description;
    private int handleTag;
    private String handleOwner;

    public InvokeDynamicInsn(){
    }

    public InvokeDynamicInsn(String name, String description, int handleTag, String handleOwner){
        setName(name);
        setDescription(description);
        setHandleTag(handleTag);
        setHandleOwner(handleOwner);
    }

    public String getName(){
        return name;
    }

    public String getDescription(){
        return description;
    }

    public int getHandleTag(){
        return handleTag;
    }

    public String getHandleOwner(){
        return handleOwner;
    }

    public void setName(String name){
        if(name == null){
            throw new NullPointerException();
        }
        this.name = name;
    }

    public void setHandleOwner(String owner){
        if(owner == null){
            throw new NullPointerException();
        }
        this.handleOwner = owner;
    }

    public void setDescription(String description){
        if(description == null){
            throw new NullPointerException();
        }
        this.description = description;
    }

    public void setHandleTag(int handleTag){
        if(handleTag <= 0){
            throw new IllegalArgumentException();
        }
        this.handleTag = handleTag;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(getName()).append(", ").append(getDescription()).append(", ");
        sb.append(getHandleTag()).append(", ").append(getHandleOwner());

        return new String(sb);
    }
}
