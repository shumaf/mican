package jp.ksu.tmdlab.mican.insn;

import java.util.Arrays;

import org.objectweb.asm.Label;

public class LookupSwitchInsn implements java.io.Serializable{
    private static final long serialVersionUID = -557247664868332846L;

    private Label arg0;
    private int[] arg1;
    private Label[] arg2;

    public LookupSwitchInsn(){
    }

    public LookupSwitchInsn(Label arg0, int[] origArg1, Label[] origArg2){
        setDefault(arg0);
        setKeys(origArg1);
        setLabels(origArg2);
    }

    public void setDefault(Label dflt){
        this.arg0 = dflt;
    }

    public void setKeys(int[] keys){
        arg1 = new int[keys.length];
        System.arraycopy(keys, 0, arg1, 0, keys.length);
    }

    public void setLabels(Label[] labels){
        arg2 = new Label[labels.length];
        System.arraycopy(labels, 0, arg2, 0, labels.length);
    }

    public Label getDefault(){
        return arg0;
    }

    public int[] getKeys(){
        int[] returnKeys = new int[arg1.length];
        System.arraycopy(arg1, 0, returnKeys, 0, arg1.length);

        return returnKeys;
    }

    public Label[] getLabels(){
        Label[] labels = new Label[arg2.length];
        System.arraycopy(arg2, 0, labels, 0, arg2.length);

        return labels;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(arg0.toString()).append(", ");
        sb.append(Arrays.toString(arg1)).append(", ");
        sb.append(Arrays.toString(arg2));

        return new String(sb);
    }
}
