package jp.ksu.tmdlab.mican;

import org.objectweb.asm.Label;
import org.objectweb.asm.Opcodes;

public class OpcodeInfo {
    private int opcode;
    private int count;
    private Operand<?> operand = null;

    public OpcodeInfo(int count, int opcode) {
        this.opcode = opcode;
        this.count = count;
        this.operand = new NoOperand();
    }

    public OpcodeInfo(int count, int opcode, int arg1, int arg2) {
        this(count, opcode);
        this.operand = new IincOperand(arg1, arg2);
    }

    public OpcodeInfo(int count, int opcode, String arg1, String arg2, String arg3) {
        this(count, opcode);
        this.operand = new InvokeMethodOperand(arg1, arg2, arg3);
    }

    public OpcodeInfo(int count, int opcode, int arg) {
        this(count, opcode);
        this.operand = new IntegerOperand(arg);
    }

    public OpcodeInfo(int count, int opcode, Label label) {
        this(count, opcode);
        this.operand = new JumpOperand(label);
    }

    public OpcodeInfo(int count, int opcode, Object obj) {
        this(count, opcode);
        this.operand = new LdcOperand(obj);
    }

    public OpcodeInfo(int count, int opcode, Label arg0, int[] arg1, Label[] arg2) {
        this(count, opcode);
        this.operand = new LookupSwitchOperand(arg0, arg1, arg2);
    }

    public OpcodeInfo(int count, int opcode, String desc, int dims) {
        this(count, opcode);
        this.operand = new MultiANewArrayOperand(desc, dims);
    }

    public OpcodeInfo(int count, int opcode, int min, int max, Label dflt, Label... labels) {
        this(count, opcode);
        this.operand = new TableSwitchOperand(min, max, dflt, labels);
    }

    public OpcodeInfo(int count, int opcode, String arg) {
        this(count, opcode);
        this.operand = new TypeOperand(arg);
    }

    public OpcodeInfo(int count, String arg1, String arg2, int arg3, String arg4) {
        this(count, org.objectweb.asm.Opcodes.INVOKEDYNAMIC);
        this.operand = new InvokeDynamicOperand(arg1, arg2, arg3, arg4);
    }

    void removeOpcode(){
        opcode = -1;
    }

    public int getOpcode(){
        return opcode;
    }

    public int getCount(){
        return count;
    }

    public Operand<?> getOperand(){
        return operand;
    }

    public void increment(){
        count++;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(count + " : ");
        switch(this.opcode) {
            case Opcodes.AALOAD: sb.append("AALOAD "); break;
            case Opcodes.AASTORE: sb.append("AASTORE "); break;
            // case Opcodes.ACC_ABSTRACT: sb.append("ACC_ABSTRACT "); break;
            // case Opcodes.ACC_ANNOTATION: sb.append("ACC_ANNOTATION "); break;
            // case Opcodes.ACC_BRIDGE: sb.append("ACC_BRIDGE "); break;
            // case Opcodes.ACC_DEPRECATED: sb.append("ACC_DEPRECATED "); break;
            // case Opcodes.ACC_ENUM: sb.append("ACC_ENUM "); break;
            // case Opcodes.ACC_FINAL: sb.append("ACC_FINAL "); break;
            // case Opcodes.ACC_INTERFACE: sb.append("ACC_INTERFACE "); break;
            // case Opcodes.ACC_MANDATED: sb.append("ACC_MANDATED "); break;
            // case Opcodes.ACC_NATIVE: sb.append("ACC_NATIVE "); break;
            // case Opcodes.ACC_PRIVATE: sb.append("ACC_PRIVATE "); break;
            // case Opcodes.ACC_PROTECTED: sb.append("ACC_PROTECTED "); break;
            // case Opcodes.ACC_PUBLIC: sb.append("ACC_PUBLIC "); break;
            // case Opcodes.ACC_STATIC: sb.append("ACC_STATIC "); break;
            // case Opcodes.ACC_STRICT: sb.append("ACC_STRICT "); break;
            // case Opcodes.ACC_SUPER: sb.append("ACC_SUPER "); break;
            // case Opcodes.ACC_SYNCHRONIZED: sb.append("ACC_SYNCHRONIZED "); break;
            // case Opcodes.ACC_SYNTHETIC: sb.append("ACC_SYNTHETIC "); break;
            // case Opcodes.ACC_TRANSIENT: sb.append("ACC_TRANSIENT "); break;
            // case Opcodes.ACC_VARARGS: sb.append("ACC_VARARGS "); break;
            // case Opcodes.ACC_VOLATILE: sb.append("ACC_VOLATILE "); break;
            case Opcodes.ACONST_NULL: sb.append("ACONST_NULL "); break;
            case Opcodes.ALOAD: sb.append("ALOAD "); break;
            case Opcodes.ANEWARRAY: sb.append("ANEWARRAY "); break;
            case Opcodes.ARETURN: sb.append("ARETURN "); break;
            case Opcodes.ARRAYLENGTH: sb.append("ARRAYLENGTH "); break;
            case Opcodes.ASM4: sb.append("ASM4 "); break;
            case Opcodes.ASM5: sb.append("ASM5 "); break;
            case Opcodes.ASTORE: sb.append("ASTORE "); break;
            case Opcodes.ATHROW: sb.append("ATHROW "); break;
            case Opcodes.BALOAD: sb.append("BALOAD "); break;
            case Opcodes.BASTORE: sb.append("BASTORE "); break;
            case Opcodes.BIPUSH: sb.append("BIPUSH "); break;
            case Opcodes.CALOAD: sb.append("CALOAD "); break;
            case Opcodes.CASTORE: sb.append("CASTORE "); break;
            case Opcodes.CHECKCAST: sb.append("CHECKCAST "); break;
            case Opcodes.D2F: sb.append("D2F "); break;
            case Opcodes.D2I: sb.append("D2I "); break;
            case Opcodes.D2L: sb.append("D2L "); break;
            case Opcodes.DADD: sb.append("DADD "); break;
            case Opcodes.DALOAD: sb.append("DALOAD "); break;
            case Opcodes.DASTORE: sb.append("DASTORE "); break;
            case Opcodes.DCMPG: sb.append("DCMPG "); break;
            case Opcodes.DCMPL: sb.append("DCMPL "); break;
            case Opcodes.DCONST_0: sb.append("DCONST_0 "); break;
            case Opcodes.DCONST_1: sb.append("DCONST_1 "); break;
            case Opcodes.DDIV: sb.append("DDIV "); break;
            case Opcodes.DLOAD: sb.append("DLOAD "); break;
            case Opcodes.DMUL: sb.append("DMUL "); break;
            case Opcodes.DNEG: sb.append("DNEG "); break;
            // case Opcodes.DOUBLE: sb.append("DOUBLE "); break;
            case Opcodes.DREM: sb.append("DREM "); break;
            case Opcodes.DRETURN: sb.append("DRETURN "); break;
            case Opcodes.DSTORE: sb.append("DSTORE "); break;
            case Opcodes.DSUB: sb.append("DSUB "); break;
            case Opcodes.DUP: sb.append("DUP "); break;
            case Opcodes.DUP_X1: sb.append("DUP_X1 "); break;
            case Opcodes.DUP_X2: sb.append("DUP_X2 "); break;
            case Opcodes.DUP2: sb.append("DUP2 "); break;
            case Opcodes.DUP2_X1: sb.append("DUP2_X1 "); break;
            case Opcodes.DUP2_X2: sb.append("DUP2_X2 "); break;
            // case Opcodes.F_APPEND: sb.append("F_APPEND"); break;
            // case Opcodes.F_CHOP: sb.append("F_CHOP"); break;
            // case Opcodes.F_FULL: sb.append("F_FULL"); break;
            // case Opcodes.F_NEW: sb.append("F_NEW"); break;
            // case Opcodes.F_SAME: sb.append("F_SAME"); break;
            // case Opcodes.F_SAME1: sb.append("F_SAME1"); break;
            case Opcodes.F2D: sb.append("F2D "); break;
            case Opcodes.F2I: sb.append("F2I "); break;
            case Opcodes.F2L: sb.append("F2L "); break;
            case Opcodes.FADD: sb.append("FADD "); break;
            case Opcodes.FALOAD: sb.append("FALOAD "); break;
            case Opcodes.FASTORE: sb.append("FASTORE "); break;
            case Opcodes.FCMPG: sb.append("FCMPG "); break;
            case Opcodes.FCMPL: sb.append("FCMPL "); break;
            case Opcodes.FCONST_0: sb.append("FCONST_0 "); break;
            case Opcodes.FCONST_1: sb.append("FCONST_1 "); break;
            case Opcodes.FCONST_2: sb.append("FCONST_2 "); break;
            case Opcodes.FDIV: sb.append("FDIV "); break;
            case Opcodes.FLOAD: sb.append("FLOAD "); break;
            // case Opcodes.FLOAT: sb.append("FLOAT "); break;
            case Opcodes.FMUL: sb.append("FMUL "); break;
            case Opcodes.FNEG: sb.append("FNEG "); break;
            case Opcodes.FREM: sb.append("FREM "); break;
            case Opcodes.FRETURN: sb.append("FRETURN "); break;
            case Opcodes.FSTORE: sb.append("FSTORE "); break;
            case Opcodes.FSUB: sb.append("FSUB "); break;
            case Opcodes.GETFIELD: sb.append("GETFIELD "); break;
            case Opcodes.GETSTATIC: sb.append("GETSTATIC "); break;
            case Opcodes.GOTO: sb.append("GOTO "); break;
            // case Opcodes.H_GETFIELD: sb.append("H_GETFIELD "); break;
            // case Opcodes.H_GETSTATIC: sb.append("H_GETSTATIC "); break;
            // case Opcodes.H_INVOKEINTERFACE: sb.append("H_INVOKEINTERFACE "); break;
            // case Opcodes.H_INVOKESPECIAL: sb.append("H_INVOKESPECIAL "); break;
            // case Opcodes.H_INVOKESTATIC: sb.append("H_INVOKESTATIC "); break;
            // case Opcodes.H_INVOKEVIRTUAL: sb.append("H_INVOKEVIRTUAL "); break;
            // case Opcodes.H_NEWINVOKESPECIAL: sb.append("H_NEWINVOKESPECIAL "); break;
            // case Opcodes.H_PUTFIELD: sb.append("H_PUTFIELD "); break;
            // case Opcodes.H_PUTSTATIC: sb.append("H_PUTSTATIC "); break;
            case Opcodes.I2B: sb.append("I2B "); break;
            case Opcodes.I2C: sb.append("I2C "); break;
            case Opcodes.I2D: sb.append("I2D "); break;
            case Opcodes.I2F: sb.append("I2F "); break;
            case Opcodes.I2L: sb.append("I2L "); break;
            case Opcodes.I2S: sb.append("I2S "); break;
            case Opcodes.IADD: sb.append("IADD "); break;
            case Opcodes.IALOAD: sb.append("IALOAD "); break;
            case Opcodes.IAND: sb.append("IAND "); break;
            case Opcodes.IASTORE: sb.append("IASTORE "); break;
            case Opcodes.ICONST_0: sb.append("ICONST_0 "); break;
            case Opcodes.ICONST_1: sb.append("ICONST_1 "); break;
            case Opcodes.ICONST_2: sb.append("ICONST_2 "); break;
            case Opcodes.ICONST_3: sb.append("ICONST_3 "); break;
            case Opcodes.ICONST_4: sb.append("ICONST_4 "); break;
            case Opcodes.ICONST_5: sb.append("ICONST_5 "); break;
            case Opcodes.ICONST_M1: sb.append("ICONST_M1 "); break;
            case Opcodes.IDIV: sb.append("IDIV "); break;
            case Opcodes.IF_ACMPEQ: sb.append("IF_ACMPEQ "); break;
            case Opcodes.IF_ACMPNE: sb.append("IF_ACMPNE "); break;
            case Opcodes.IF_ICMPEQ: sb.append("IF_ICMPEQ "); break;
            case Opcodes.IF_ICMPGE: sb.append("IF_ICMPGE "); break;
            case Opcodes.IF_ICMPGT: sb.append("IF_ICMPGT "); break;
            case Opcodes.IF_ICMPLE: sb.append("IF_ICMPLE "); break;
            case Opcodes.IF_ICMPLT: sb.append("IF_ICMPLT "); break;
            case Opcodes.IF_ICMPNE: sb.append("IF_ICMPNE "); break;
            case Opcodes.IFEQ: sb.append("IFEQ "); break;
            case Opcodes.IFGE: sb.append("IFGE "); break;
            case Opcodes.IFGT: sb.append("IFGT "); break;
            case Opcodes.IFLE: sb.append("IFLE "); break;
            case Opcodes.IFLT: sb.append("IFLT "); break;
            case Opcodes.IFNE: sb.append("IFNE "); break;
            case Opcodes.IFNONNULL: sb.append("IFNONNULL "); break;
            case Opcodes.IFNULL: sb.append("IFNULL "); break;
            case Opcodes.IINC: sb.append("IINC "); break;
            case Opcodes.ILOAD: sb.append("ILOAD "); break;
            case Opcodes.IMUL: sb.append("IMUL "); break;
            case Opcodes.INEG: sb.append("INEG "); break;
            case Opcodes.INSTANCEOF: sb.append("INSTANCEOF "); break;
            // case Opcodes.INTEGER: sb.append("INTEGER "); break;
            case Opcodes.INVOKEDYNAMIC: sb.append("INVOKEDYNAMIC "); break;
            case Opcodes.INVOKEINTERFACE: sb.append("INVOKEINTERFACE "); break;
            case Opcodes.INVOKESPECIAL: sb.append("INVOKESPECIAL "); break;
            case Opcodes.INVOKESTATIC: sb.append("INVOKESTATIC "); break;
            case Opcodes.INVOKEVIRTUAL: sb.append("INVOKEVIRTUAL "); break;
            case Opcodes.IOR: sb.append("IOR "); break;
            case Opcodes.IREM: sb.append("IREM "); break;
            case Opcodes.IRETURN: sb.append("IRETURN "); break;
            case Opcodes.ISHL: sb.append("ISHL "); break;
            case Opcodes.ISHR: sb.append("ISHR "); break;
            case Opcodes.ISTORE: sb.append("ISTORE "); break;
            case Opcodes.ISUB: sb.append("ISUB "); break;
            case Opcodes.IUSHR: sb.append("IUSHR "); break;
            case Opcodes.IXOR: sb.append("IXOR "); break;
            case Opcodes.JSR: sb.append("JSR "); break;
            case Opcodes.L2D: sb.append("L2D "); break;
            case Opcodes.L2F: sb.append("L2F "); break;
            case Opcodes.L2I: sb.append("L2I "); break;
            case Opcodes.LADD: sb.append("LADD "); break;
            case Opcodes.LALOAD: sb.append("LALOAD "); break;
            case Opcodes.LAND: sb.append("LAND "); break;
            case Opcodes.LASTORE: sb.append("LASTORE "); break;
            case Opcodes.LCMP: sb.append("LCMP "); break;
            case Opcodes.LCONST_0: sb.append("LCONST_0 "); break;
            case Opcodes.LCONST_1: sb.append("LCONST_1 "); break;
            case Opcodes.LDC: sb.append("LDC "); break;
            case Opcodes.LDIV: sb.append("LDIV "); break;
            case Opcodes.LLOAD: sb.append("LLOAD "); break;
            case Opcodes.LMUL: sb.append("LMUL "); break;
            case Opcodes.LNEG: sb.append("LNEG "); break;
            // case Opcodes.LONG: sb.append("LONG "); break;
            case Opcodes.LOOKUPSWITCH: sb.append("LOOKUPSWITCH "); break;
            case Opcodes.LOR: sb.append("LOR "); break;
            case Opcodes.LREM: sb.append("LREM "); break;
            case Opcodes.LRETURN: sb.append("LRETURN "); break;
            case Opcodes.LSHL: sb.append("LSHL "); break;
            case Opcodes.LSHR: sb.append("LSHR "); break;
            case Opcodes.LSTORE: sb.append("LSTORE "); break;
            case Opcodes.LSUB: sb.append("LSUB "); break;
            case Opcodes.LUSHR: sb.append("LUSHR "); break;
            case Opcodes.LXOR: sb.append("LXOR "); break;
            case Opcodes.MONITORENTER: sb.append("MONITORENTER "); break;
            case Opcodes.MONITOREXIT: sb.append("MONITOREXIT "); break;
            case Opcodes.MULTIANEWARRAY: sb.append("MULTIANEWARRAY "); break;
            case Opcodes.NEW: sb.append("NEW "); break;
            case Opcodes.NEWARRAY: sb.append("NEWARRAY "); break;
            case Opcodes.NOP: sb.append("NOP "); break;
            // case Opcodes.NULL: sb.append("NULL "); break;
            case Opcodes.POP: sb.append("POP "); break;
            case Opcodes.POP2: sb.append("POP2 "); break;
            case Opcodes.PUTFIELD: sb.append("PUTFIELD "); break;
            case Opcodes.PUTSTATIC: sb.append("PUTSTATIC "); break;
            case Opcodes.RET: sb.append("RET "); break;
            case Opcodes.RETURN: sb.append("RETURN "); break;
            case Opcodes.SALOAD: sb.append("SALOAD "); break;
            case Opcodes.SASTORE: sb.append("SASTORE "); break;
            case Opcodes.SIPUSH: sb.append("SIPUSH "); break;
            case Opcodes.SWAP: sb.append("SWAP "); break;
            // case Opcodes.T_BOOLEAN: sb.append("T_BOOLEAN "); break;
            // case Opcodes.T_BYTE: sb.append("T_BYTE "); break;
            // case Opcodes.T_CHAR: sb.append("T_CHAR "); break;
            // case Opcodes.T_DOUBLE: sb.append("T_DOUBLE "); break;
            // case Opcodes.T_FLOAT: sb.append("T_FLOAT "); break;
            // case Opcodes.T_INT: sb.append("T_INT "); break;
            // case Opcodes.T_LONG: sb.append("T_LONG "); break;
            // case Opcodes.T_SHORT: sb.append("T_SHORT "); break;
            case Opcodes.TABLESWITCH: sb.append("TABLESWITCH "); break;
            // case Opcodes.TOP: sb.append("TOP "); break;
            // case Opcodes.UNINITIALIZED_THIS: sb.append("UNINITIALIZED_THIS "); break;
            // case Opcodes.V1_1: sb.append("V1_1 "); break;
            // case Opcodes.V1_2: sb.append("V1_2 "); break;
            // case Opcodes.V1_3: sb.append("V1_3 "); break;
            // case Opcodes.V1_4: sb.append("V1_4 "); break;
            // case Opcodes.V1_5: sb.append("V1_5 "); break;
            // case Opcodes.V1_6: sb.append("V1_6 "); break;
            // case Opcodes.V1_7: sb.append("V1_7 "); break; 
            // case Opcodes.V1_8: sb.append("V _8 "); break;
        }
        sb.append("\t\t");
        sb.append(this.operand.toString());
        sb.append(System.getProperty("line.separator"));
        return sb.toString();
    }
}
