package jp.ksu.tmdlab.mican;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Main class<br/>
 *
 * @author shuma.F
 * @version 0.1
 * @since 0.1
 */
public class Main {
    private static final int BUFFER_SIZE = 256;

    /**
     * Constractor of Main．
     * 
     * @param args
     *            Arguments(Probably... Command Line Arguments)
     * @since 0.1
     */
    public Main(String[] args) {
        try {
            ClassLoader loader = buildClassLoader(args);
            for (String file : args) {
                perform(file, loader);
            }
        } catch (IOException e) {
        }
    }

    /**
     * ClassLoader is built form arguments．
     * 
     * @param args
     *            Filename
     * @return Built ClassLoader
     * @since 0.1
     */
    private ClassLoader buildClassLoader(String[] args) throws IOException {
        List<URL> urls = new ArrayList<URL>();
        for (String arg : args) {
            urls.add(new File(arg).toURI().toURL());
        }
        return new URLClassLoader(urls.toArray(new URL[urls.size()]));
    }

    /**
     * performes transformation/calculation with one file.
     * 
     * @param transformer
     *            任意の操作をするTransformer.
     * @param file
     *            byte code data of target class.
     * @param loader
     *            ClassLoader that already file was loadid
     * @throws IOException
     *             I/O error.
     */
    private void perform(final String file, final ClassLoader loader)
            throws IOException {
        byte[] original = getData(file);
        String className = ClassNameExtractVisitor.parseClassName(original);

        Map<String, List<OpcodeInfo>> map = calculate(className, original);
        System.out.println(map);

        for (OpcodeInfo i : map.get("Hello_main")) {
            OpcodeDiff od = (OpcodeDiff) i;

            if (i.getCount() == 2) {
                od.addBefore(new OpcodeInfo(1, org.objectweb.asm.Opcodes.LDC,
                        " world"));
                od.addBefore(new OpcodeInfo(1, od.getOpcode(), "java/lang/String",
                        "concat", "(Ljava/lang/String;)Ljava/lang/String;"));
                od.change(new OpcodeInfo(1, od.getOpcode(), "java/io/PrintStream",
                        "println", "(Ljava/lang/String;)V"));
            } else if (i.getCount() == 3) {
                // ((OpcodeDiff)i).remove();
            } else if (i.getCount() == 4) {
                // ((OpcodeDiff)i).remove();
            } else if (i.getCount() == 5) {
                od.change(new OpcodeInfo(1, od.getOpcode(), 1, 2));
            } else if (i.getCount() == 10) {
                od.addNext(new OpcodeInfo(1, org.objectweb.asm.Opcodes.IADD));
            }
        }

        TransformFilter filter = null;
        OpcodeExtractionTransformer transformer = new OpcodeExtractionTransformer(
                filter, map);

        transform(transformer, className, original, loader);
    }

    private Map<String, List<OpcodeInfo>> calculate(String className,
            byte[] data) throws UnsupportedEncodingException {
        Map<String, List<OpcodeInfo>> rs = StaticallyOpcodeExtractVisitor
                .parse(data);
        return rs;
    }

    private byte[] getData(final String file) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        InputStream in = null;
        try {
            in = new FileInputStream(file);
            int read;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            out.close();
            return out.toByteArray();
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }

    private void transform(final OpcodeExtractionTransformer transformer,
            final String className, final byte[] data, final ClassLoader loader) {
        byte[] transformed = transformer.transform(className, data, loader);
        if (transformed == null) {
            transformed = data;
        }
        transformer.output("dest", className, transformed);
    }

    /**
     * main method．
     * 
     * @param args
     *            Command Line Arguments
     * @since 0.1
     */
    public static void main(String[] args) {
        new Main(args);
    }
}
