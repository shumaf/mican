package jp.ksu.tmdlab.mican;

import jp.ksu.tmdlab.mican.insn.IincInsn;
import jp.ksu.tmdlab.mican.insn.InvokeDynamicInsn;
import jp.ksu.tmdlab.mican.insn.InvokeMethodInsn;
import jp.ksu.tmdlab.mican.insn.LookupSwitchInsn;
import jp.ksu.tmdlab.mican.insn.MultiANewArrayInsn;
import jp.ksu.tmdlab.mican.insn.TableSwitchInsn;

import org.objectweb.asm.Label;

public abstract class Operand<T>{
    T value;

    public Operand(T value){
        this.value = value;
    }

    public final T getValue(){
        return value;
    }

    @Override
    public String toString(){
        return getValue().toString();
    }
}

class NoOperand extends Operand<Void>{
    public NoOperand(){
        super(null);
    }

    @Override
    public String toString(){
        return "";
    }
}

class InvokeMethodOperand extends Operand<InvokeMethodInsn>{
    public InvokeMethodOperand(String arg1, String arg2, String arg3){
        super(new InvokeMethodInsn(arg1, arg2, arg3));
    }

    public String getOwner(){
        return getValue().getOwner();
    }

    public String getName(){
        return getValue().getName();
    }

    public String getDescription(){
        return getValue().getDescription();
    }
}

class InvokeDynamicOperand extends Operand<InvokeDynamicInsn>{
    public InvokeDynamicOperand(String arg1, String arg2, int arg3, String arg4){
        super(new InvokeDynamicInsn(arg1, arg2, arg3, arg4));
    }

    public String getName(){
        return getValue().getName();
    }

    public String getDescription(){
        return getValue().getDescription();
    }

    public int getHandleTag(){
        return getValue().getHandleTag();
    }

    public String getHandleOwner(){
        return getValue().getHandleOwner();
    }
}

class IincOperand extends Operand<IincInsn>{
    public IincOperand(int var, int increment){
        super(new IincInsn(var, increment));
    }

    public int getVar(){
        return getValue().getVar();
    }

    public int getIncrement(){
        return getValue().getIncrement();
    }
}

class IntegerOperand extends Operand<Integer>{
    public IntegerOperand(int arg){
        super(arg);
    }
}

class TypeOperand extends Operand<String>{
    public TypeOperand(String string){
        super(string);
    }
}

class LdcOperand extends Operand<Object>{
    public LdcOperand(Object object){
        super(object);
    }
}

class JumpOperand extends Operand<Label>{
    public JumpOperand(Label label){
        super(label);
    }
}

class LookupSwitchOperand extends Operand<LookupSwitchInsn>{
    public LookupSwitchOperand(Label arg0, int[] origArg1, Label[] origArg2){
        super(new LookupSwitchInsn(arg0, origArg1, origArg2));
    }

    public Label getDefault(){
        return getValue().getDefault();
    }

    public int[] getKeys(){
        return getValue().getKeys();
    }

    public Label[] getLabels(){
        return getValue().getLabels();
    }
}

class TableSwitchOperand extends Operand<TableSwitchInsn> {
    public TableSwitchOperand(int min, int max, Label dflt, Label[] labels){
        super(new TableSwitchInsn(min, max, dflt, labels));
    }

    public int getMin(){
        return getValue().getMin();
    }

    public int getMax(){
        return getValue().getMin();
    }

    public Label getDefault(){
        return getValue().getDefault();
    }

    public Label[] getLabels(){
        return getValue().getLabels();
    }
}

class MultiANewArrayOperand extends Operand<MultiANewArrayInsn> {
    public MultiANewArrayOperand(String arg0, int arg1){
        super(new MultiANewArrayInsn(arg0, arg1));
    }

    public String getDescription(){
        return getValue().getDescription();
    }

    public int getDims(){
        return getValue().getDims();
    }
}

