package jp.ksu.tmdlab.mican;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 * Opcode extract visitor for static analysis. For easy use, this class shows
 * {@link #parse <code>parse</code>} method.
 * 
 * @author Haruaki Tamada
 */
public class StaticallyOpcodeExtractVisitor extends ClassVisitor {
    private Map<String, List<OpcodeInfo>> map = new HashMap<String, List<OpcodeInfo>>();
    private String className;

    public StaticallyOpcodeExtractVisitor() {
        super(Opcodes.ASM4);
    }

    public StaticallyOpcodeExtractVisitor(ClassVisitor visitor) {
        super(Opcodes.ASM4, visitor);
    }

    public Map<String, List<OpcodeInfo>> getList() {
        return map;
    }

    @Override
    public void visit(final int version, final int access, final String name,
            final String signature, final String superName,
            final String[] interfaces) {
        this.className = name;
        super.visit(version, access, name, signature, superName, interfaces);
    }

    @Override
    public MethodVisitor visitMethod(final int access, final String name,
            final String desc, final String signature, final String[] exceptions) {
        MethodVisitor visitor = super.visitMethod(access, name, desc,
                signature, exceptions);

        OpcodeExtractMethodVisitor mv = new OpcodeExtractMethodVisitor(visitor,
                className, name);
        map.put(className + "_" + name, mv.oplog);
        return mv;
    }

    public static Map<String, List<OpcodeInfo>> parse(byte[] data) {
        ClassReader reader = new ClassReader(data);
        StaticallyOpcodeExtractVisitor visitor = new StaticallyOpcodeExtractVisitor();
        reader.accept(visitor, 0);

        return visitor.getList();
    }

    private static final class OpcodeExtractMethodVisitor extends MethodVisitor {
        private List<OpcodeInfo> oplog;
        private int count = 0;

        public OpcodeExtractMethodVisitor(final MethodVisitor visitor,
                final String className, final String callee) {
            super(Opcodes.ASM4, visitor);
            // oplog = new OpcodeEditList(className, callee, "<static>");
            oplog = new ArrayList<OpcodeInfo>();
        }

        @Override
        public void visitFieldInsn(int opcode, String arg1, String arg2,
                String arg3) {
            oplog.add(new OpcodeDiff(count, opcode, arg1, arg2, arg3));
            count++;
            super.visitFieldInsn(opcode, arg1, arg2, arg3);
        }

        @Override
        public void visitCode() {
            super.visitCode();
        }

        @Override
        public void visitIincInsn(int opcode, int arg1) {
            oplog.add(new OpcodeDiff(count, Opcodes.IINC, opcode, arg1));
            count++;
            super.visitIincInsn(opcode, arg1);
        }

        @Override
        public void visitInsn(int opcode) {
            oplog.add(new OpcodeDiff(count, opcode));
            count++;
            super.visitInsn(opcode);
        }

        @Override
        public void visitIntInsn(int opcode, int operand) {
            oplog.add(new OpcodeDiff(count, opcode, operand));
            count++;
            super.visitIntInsn(opcode, operand);
        }

        @Override
        public void visitJumpInsn(int opcode, Label label) {
            oplog.add(new OpcodeDiff(count, opcode, label));
            count++;
            super.visitJumpInsn(opcode, label);
        }

        @Override
        public void visitLdcInsn(Object object) {
            if (object instanceof Double || object instanceof Long) {
                oplog.add(new OpcodeDiff(count, Opcodes.LDC, object));
            } else {
                oplog.add(new OpcodeDiff(count, Opcodes.LDC, object));
            }
            count++;
            super.visitLdcInsn(object);
        }

        @Override
        public void visitLineNumber(int line, Label label) {
            // counter.visitLine(line);
            super.visitLineNumber(line, label);
        }

        @Override
        public void visitLocalVariable(String name, String desc,
                String signature, Label start, Label end, int index) {
            super.visitLocalVariable(name, desc, signature, start, end, index);
        }

        @Override
        public void visitLookupSwitchInsn(Label arg0, int[] arg1, Label[] arg2) {
            oplog.add(new OpcodeDiff(count, Opcodes.LOOKUPSWITCH, arg0, arg1,
                    arg2));
            count++;
            super.visitLookupSwitchInsn(arg0, arg1, arg2);
        }

        @Override
        public void visitMethodInsn(int opcode, String owner, String name,
                String desc) {
            oplog.add(new OpcodeDiff(count, opcode, owner, name, desc));
            count++;
            super.visitMethodInsn(opcode, owner, name, desc, opcode != Opcodes.INVOKEINTERFACE);
        }

        @Override
        public void visitMultiANewArrayInsn(String desc, int dims) {
            oplog.add(new OpcodeDiff(count, Opcodes.MULTIANEWARRAY, desc, dims));
            count++;
            super.visitMultiANewArrayInsn(desc, dims);
        }

        @Override
        public void visitTableSwitchInsn(int min, int max, Label dflt,
                Label... labels) {
            oplog.add(new OpcodeDiff(count, Opcodes.TABLESWITCH, min, max,
                    dflt, labels));
            count++;
            super.visitTableSwitchInsn(min, max, dflt, labels);
        }

        @Override
        public void visitTypeInsn(int opcode, String type) {
            oplog.add(new OpcodeDiff(count, opcode, type));
            count++;
            super.visitTypeInsn(opcode, type);
        }

        @Override
        public void visitVarInsn(int opcode, int var) {
            oplog.add(new OpcodeDiff(count, opcode, var));
            count++;
            super.visitVarInsn(opcode, var);
        }
    }
}
