package jp.ksu.tmdlab.mican;

import java.lang.invoke.CallSite;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.MethodType;
import java.util.List;
import java.util.Map;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Handle;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 * This class is visitor class for ASM, and weaves the code to extract opcode
 * sequence into target class.
 *
 * @author Haruaki Tamada
 */
public final class OpcodeExtractVisitor extends ClassVisitor {
    private String className;
    Map<String, List<OpcodeInfo>> list;

    public OpcodeExtractVisitor(final ClassVisitor visitor,
            Map<String, List<OpcodeInfo>> list) {
        super(Opcodes.ASM4, visitor);
        this.list = list;
    }

    public String getClassName() {
        return className;
    }

    @Override
    public void visit(final int version, final int access, final String name,
            final String signature, final String superName,
            final String[] interfaces) {
        this.className = name;
        super.visit(version, access, name, signature, superName, interfaces);
    }

    @Override
    public MethodVisitor visitMethod(final int access, final String name,
            final String desc, final String signature, final String[] exceptions) {
        MethodVisitor visitor = super.visitMethod(access, name, desc,
                signature, exceptions);
        // System.out.println(className+"_"+name);
        return new OpcodeExtractMethodVisitor(visitor, className, name,
                this.list.get(className + "_" + name));
    }

    private static final class OpcodeExtractMethodVisitor extends MethodVisitor {
        List<OpcodeInfo> oelist = null;
        int count = 0;

        public OpcodeExtractMethodVisitor(final MethodVisitor visitor,
                final String className, final String callee,
                final List<OpcodeInfo> oelist) {
            super(Opcodes.ASM4, visitor);
            this.oelist = oelist;
            count = 0;
        }

        public boolean trans(int count) {
            // System.out.println(count);
            OpcodeDiff diff = (OpcodeDiff) this.oelist.get(count);
            if (diff.isTrans()) {
                if (diff.before.size() != 0) {
                    for (OpcodeInfo i : diff.before) {
                        execute(i);
                    }
                }
                if (diff.getOpcode() == -1) {
                    System.out.println("ok");
                    // do nothing...
                } else if (diff.getCurrent() != null) {
                    execute(diff.getCurrent());
                } else {
                    execute(diff);
                }
                if (diff.after.size() != 0) {
                    for (OpcodeInfo i : diff.after) {
                        execute(i);
                    }
                }
                return false;
            } else {
                return true;
            }
        }

        public void execute(OpcodeInfo action) {
            int opcode = action.getOpcode();
            switch (opcode) {
            case Opcodes.IINC: {
                IincOperand iinc = (IincOperand) action.getOperand();
                super.visitIincInsn(iinc.getVar(), iinc.getIncrement());
                break;
            }
            case Opcodes.ICONST_5:
            case Opcodes.LCONST_0:
            case Opcodes.LCONST_1:
            case Opcodes.FCONST_0:
            case Opcodes.FCONST_1:
            case Opcodes.FCONST_2:
            case Opcodes.DCONST_0:
            case Opcodes.DCONST_1:
            case Opcodes.IALOAD:
            case Opcodes.LALOAD:
            case Opcodes.FALOAD:
            case Opcodes.DALOAD:
            case Opcodes.AALOAD:
            case Opcodes.BALOAD:
            case Opcodes.CALOAD:
            case Opcodes.SALOAD:
            case Opcodes.IASTORE:
            case Opcodes.LASTORE:
            case Opcodes.FASTORE:
            case Opcodes.DASTORE:
            case Opcodes.AASTORE:
            case Opcodes.BASTORE:
            case Opcodes.CASTORE:
            case Opcodes.SASTORE:
            case Opcodes.POP:
            case Opcodes.POP2:
            case Opcodes.DUP:
            case Opcodes.DUP_X1:
            case Opcodes.DUP_X2:
            case Opcodes.DUP2:
            case Opcodes.DUP2_X1:
            case Opcodes.DUP2_X2:
            case Opcodes.SWAP:
            case Opcodes.IADD:
            case Opcodes.LADD:
            case Opcodes.FADD:
            case Opcodes.DADD:
            case Opcodes.ISUB:
            case Opcodes.LSUB:
            case Opcodes.FSUB:
            case Opcodes.DSUB:
            case Opcodes.IMUL:
            case Opcodes.LMUL:
            case Opcodes.FMUL:
            case Opcodes.DMUL:
            case Opcodes.IDIV:
            case Opcodes.LDIV:
            case Opcodes.FDIV:
            case Opcodes.DDIV:
            case Opcodes.IREM:
            case Opcodes.LREM:
            case Opcodes.FREM:
            case Opcodes.DREM:
            case Opcodes.INEG:
            case Opcodes.LNEG:
            case Opcodes.FNEG:
            case Opcodes.DNEG:
            case Opcodes.ISHL:
            case Opcodes.LSHL:
            case Opcodes.ISHR:
            case Opcodes.LSHR:
            case Opcodes.IUSHR:
            case Opcodes.LUSHR:
            case Opcodes.IAND:
            case Opcodes.LAND:
            case Opcodes.IOR:
            case Opcodes.LOR:
            case Opcodes.IXOR:
            case Opcodes.LXOR:
            case Opcodes.I2L:
            case Opcodes.I2F:
            case Opcodes.I2D:
            case Opcodes.L2I:
            case Opcodes.L2F:
            case Opcodes.L2D:
            case Opcodes.F2I:
            case Opcodes.F2L:
            case Opcodes.F2D:
            case Opcodes.D2I:
            case Opcodes.D2L:
            case Opcodes.D2F:
            case Opcodes.I2B:
            case Opcodes.I2C:
            case Opcodes.I2S:
            case Opcodes.LCMP:
            case Opcodes.FCMPL:
            case Opcodes.FCMPG:
            case Opcodes.DCMPL:
            case Opcodes.DCMPG:
            case Opcodes.IRETURN:
            case Opcodes.LRETURN:
            case Opcodes.FRETURN:
            case Opcodes.DRETURN:
            case Opcodes.ARETURN:
            case Opcodes.RETURN:
            case Opcodes.ARRAYLENGTH:
            case Opcodes.ATHROW:
            case Opcodes.MONITORENTER:
            case Opcodes.MONITOREXIT: {
                super.visitInsn(opcode);
                break;
            }
            case Opcodes.BIPUSH:
            case Opcodes.SIPUSH:
            case Opcodes.NEWARRAY: {
                IntegerOperand iov = (IntegerOperand) action.getOperand();
                super.visitIntInsn(opcode, iov.getValue());
                break;
            }
            case Opcodes.IFEQ:
            case Opcodes.IFNE:
            case Opcodes.IFLT:
            case Opcodes.IFGE:
            case Opcodes.IFGT:
            case Opcodes.IFLE:
            case Opcodes.IF_ICMPEQ:
            case Opcodes.IF_ICMPNE:
            case Opcodes.IF_ICMPLT:
            case Opcodes.IF_ICMPGE:
            case Opcodes.IF_ICMPGT:
            case Opcodes.IF_ICMPLE:
            case Opcodes.IF_ACMPEQ:
            case Opcodes.IF_ACMPNE:
            case Opcodes.GOTO:
            case Opcodes.JSR:
            case Opcodes.IFNULL:
            case Opcodes.IFNONNULL: {
                JumpOperand lov = (JumpOperand) action.getOperand();
                super.visitJumpInsn(opcode, lov.getValue());
                break;
            }
            case Opcodes.LDC: {
                LdcOperand oov = (LdcOperand) action.getOperand();
                super.visitLdcInsn(oov.getValue());
                break;
            }
            case Opcodes.LOOKUPSWITCH: {
                LookupSwitchOperand lsov = (LookupSwitchOperand) action
                        .getOperand();
                super.visitLookupSwitchInsn(lsov.getDefault(), lsov.getKeys(),
                        lsov.getLabels());
                break;
            }
            case Opcodes.INVOKEVIRTUAL:
            case Opcodes.INVOKESPECIAL:
            case Opcodes.INVOKESTATIC:
            case Opcodes.INVOKEINTERFACE:
            case Opcodes.GETSTATIC:
            case Opcodes.PUTSTATIC:
            case Opcodes.GETFIELD:
            case Opcodes.PUTFIELD: {
                InvokeMethodOperand mov = (InvokeMethodOperand) action
                        .getOperand();
                super.visitMethodInsn(opcode, mov.getOwner(), mov.getName(),
                        mov.getDescription(), opcode != Opcodes.INVOKEINTERFACE);
                break;
            }
            case Opcodes.INVOKEDYNAMIC: {
                InvokeDynamicOperand dov = (InvokeDynamicOperand) action
                        .getOperand();

                super.visitInvokeDynamicInsn(
                        dov.getName(),
                        dov.getDescription(),
                        new Handle(dov.getHandleTag(), dov.getHandleOwner(),
                                "bsm", MethodType.methodType(CallSite.class,
                                        Lookup.class, String.class,
                                        MethodType.class)
                                        .toMethodDescriptorString()));
                break;
            }
            case Opcodes.MULTIANEWARRAY: {
                MultiANewArrayOperand aov = (MultiANewArrayOperand) action
                        .getOperand();
                super.visitMultiANewArrayInsn(aov.getDescription(),
                        aov.getDims());
                break;
            }
            case Opcodes.TABLESWITCH: {
                TableSwitchOperand sov = (TableSwitchOperand) action
                        .getOperand();

                super.visitTableSwitchInsn(sov.getMin(), sov.getMax(),
                        sov.getDefault(), sov.getLabels());
                break;
            }
            case Opcodes.NEW:
            case Opcodes.ANEWARRAY:
            case Opcodes.CHECKCAST:
            case Opcodes.INSTANCEOF: {
                TypeOperand ios = (TypeOperand) action.getOperand();
                super.visitTypeInsn(opcode, ios.getValue());
                break;
            }
            case Opcodes.ILOAD:
            case Opcodes.LLOAD:
            case Opcodes.FLOAD:
            case Opcodes.DLOAD:
            case Opcodes.ALOAD:
            case Opcodes.ISTORE:
            case Opcodes.LSTORE:
            case Opcodes.FSTORE:
            case Opcodes.DSTORE:
            case Opcodes.ASTORE:
            case Opcodes.RET: {
                IntegerOperand iov = (IntegerOperand) action.getOperand();
                super.visitVarInsn(opcode, iov.getValue());
                break;
            }
            }
        }

        @Override
        public void visitFieldInsn(int opcode, String arg1, String arg2,
                String arg3) {
            // oplog.add(new OpcodeDiff(opcode, arg1, arg2, arg3));
            boolean check = this.trans(count);
            if (check)
                super.visitFieldInsn(opcode, arg1, arg2, arg3);
            count++; // System.out.println("");
        }

        @Override
        public void visitIincInsn(int opcode, int arg1) {
            // oplog.add(new OpcodeDiff(opcode, arg1));
            boolean check = this.trans(count);
            if (check)
                super.visitIincInsn(opcode, arg1);
            count++; // System.out.println("");
        }

        @Override
        public void visitInsn(int opcode) {
            // oplog.add(new OpcodeDiff(opcode));
            boolean check = this.trans(count);
            if (check)
                super.visitInsn(opcode);
            count++; // System.out.println("");
        }

        @Override
        public void visitIntInsn(int opcode, int operand) {
            // oplog.add(new OpcodeDiff(opcode, operand));
            boolean check = this.trans(count);
            if (check)
                super.visitIntInsn(opcode, operand);
            count++; // System.out.println("");
        }

        @Override
        public void visitJumpInsn(int opcode, Label label) {
            // oplog.add(new OpcodeDiff(opcode, label));
            boolean check = this.trans(count);
            if (check)
                super.visitJumpInsn(opcode, label);
            count++; // System.out.println("");
        }

        @Override
        public void visitLdcInsn(Object object) {
            boolean check = this.trans(count);
            if (check)
                super.visitLdcInsn(object);
            count++; // System.out.println("");
        }

        @Override
        public void visitLineNumber(int line, Label label) {
            // counter.visitLine(line);

            super.visitLineNumber(line, label);
        }

        @Override
        public void visitLocalVariable(String name, String desc,
                String signature, Label start, Label end, int index) {
            boolean check = this.trans(count);
            if (check)
                super.visitLocalVariable(name, desc, signature, start, end,
                        index);
            count++; // System.out.println("");
        }

        @Override
        public void visitLookupSwitchInsn(Label arg0, int[] arg1, Label[] arg2) {
            // oplog.add(new OpcodeDiff(Opcodes.LOOKUPSWITCH, arg0, arg1,
            // arg2));
            boolean check = this.trans(count);
            if (check)
                super.visitLookupSwitchInsn(arg0, arg1, arg2);
            count++; // System.out.println("");
        }

        @Override
        public void visitMethodInsn(int opcode, String owner, String name,
                String desc) {
            // oplog.add(new OpcodeDiff(opcode, owner, name, desc));
            boolean check = this.trans(count);
            if (check) {
                super.visitMethodInsn(opcode, owner, name, desc,
                        opcode != Opcodes.INVOKEINTERFACE);
            }
            count++; // System.out.println("");
        }

        @Override
        public void visitMultiANewArrayInsn(String desc, int dims) {
            // oplog.add(new OpcodeDiff(Opcodes.MULTIANEWARRAY, desc, dims));
            boolean check = this.trans(count);
            if (check)
                super.visitMultiANewArrayInsn(desc, dims);
            count++; // System.out.println("");
        }

        @Override
        public void visitTableSwitchInsn(int min, int max, Label dflt,
                Label... labels) {
            // oplog.add(new OpcodeDiff(Opcodes.TABLESWITCH, min, max, dflt,
            // labels));
            boolean check = this.trans(count);
            if (check)
                super.visitTableSwitchInsn(min, max, dflt, labels);
            count++; // System.out.println("");
        }

        @Override
        public void visitTypeInsn(int opcode, String type) {
            // oplog.add(new OpcodeDiff(opcode, type));
            boolean check = this.trans(count);
            if (check)
                super.visitTypeInsn(opcode, type);
            count++; // System.out.println("");
        }

        @Override
        public void visitVarInsn(int opcode, int var) {
            // oplog.add(new OpcodeDiff(opcode, var));
            boolean check = this.trans(count);
            if (check)
                super.visitVarInsn(opcode, var);
            count++; // System.out.println("");
        }
    };
}
