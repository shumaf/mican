package jp.ksu.tmdlab.mican;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Mican {
    private static final int BUFFER_SIZE = 256;

    public Mican(String[] args) {
        try {
            ClassLoader loader = buildClassLoader(args);
            for (String file : args) {
                perform(file, loader);
            }
        } catch (IOException e) {
        }
    }

    private ClassLoader buildClassLoader(String[] args) throws IOException {
        List<URL> urls = new ArrayList<URL>();
        for (String arg : args) {
            urls.add(new File(arg).toURI().toURL());
        }
        return new URLClassLoader(urls.toArray(new URL[urls.size()]));
    }

    private void perform(final String file, final ClassLoader loader)
            throws IOException {
        byte[] original = getData(file);
        String className = ClassNameExtractVisitor.parseClassName(original);

        Map<String, List<OpcodeInfo>> map = calculate(className, original);
        for (Map.Entry<String, List<OpcodeInfo>> i : map.entrySet()) {
            System.out.println(i.getKey());
            for (OpcodeInfo j : i.getValue()) {
                int opcode = j.getOpcode();
                if (opcode == org.objectweb.asm.Opcodes.INVOKEVIRTUAL
                        || opcode == org.objectweb.asm.Opcodes.INVOKESPECIAL
                        || opcode == org.objectweb.asm.Opcodes.INVOKESTATIC
                        || opcode == org.objectweb.asm.Opcodes.INVOKEINTERFACE) {
                    System.out.println(j);
                }
            }
        }

        System.out.println("invokedynamic化したいやつはどれですか？(自分でbsmは書いてくださいね!");
        System.out.println("メソッド名と番号を指定してください！！");

        BufferedReader reader = new BufferedReader(new InputStreamReader(
                System.in));

        System.out.print("methodname : ");
        String method = reader.readLine();
        System.out.print("number : ");
        Integer number = Integer.parseInt(reader.readLine());

        System.out.print("fakename : ");
        String fakename = reader.readLine();
        System.out.print("desc : ");
        String desc = reader.readLine();
        System.out.print("bsm class : ");
        String bsm = reader.readLine();
        for (OpcodeInfo i : map.get(method)) {
            if (i.getCount() == number) {
                ((OpcodeDiff) i).change(new OpcodeInfo(1, fakename, desc,
                        org.objectweb.asm.Opcodes.H_INVOKESTATIC, bsm));
            }
        }

        TransformFilter filter = null;
        OpcodeExtractionTransformer transformer = new OpcodeExtractionTransformer(
                filter, map);

        transform(transformer, className, original, loader);
    }

    private Map<String, List<OpcodeInfo>> calculate(String className,
            byte[] data) throws UnsupportedEncodingException {
        Map<String, List<OpcodeInfo>> rs = StaticallyOpcodeExtractVisitor
                .parse(data);
        return rs;
    }

    private byte[] getData(final String file) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        InputStream in = null;
        try {
            in = new FileInputStream(file);
            int read;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            out.close();
            return out.toByteArray();
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }

    private void transform(final OpcodeExtractionTransformer transformer,
            final String className, final byte[] data, final ClassLoader loader) {
        byte[] transformed = transformer.transform(className, data, loader);
        if (transformed == null) {
            transformed = data;
        }
        transformer.output("dest", className, transformed);
    }

    public static void main(String[] args) {
        new Mican(args);
    }
}
