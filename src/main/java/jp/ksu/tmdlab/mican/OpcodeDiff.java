package jp.ksu.tmdlab.mican;

import org.objectweb.asm.Label;

import java.util.List;
import java.util.ArrayList;

public class OpcodeDiff extends OpcodeInfo{
    List<OpcodeInfo> before = new ArrayList<>();
    List<OpcodeInfo> after = new ArrayList<>();
    private OpcodeInfo current = null;

    public OpcodeDiff(int count, int opcode) {
        super(count, opcode);
    }

    public OpcodeDiff(int count, int opcode, int arg1, int arg2) {
        super(count, opcode, arg1, arg2);
    }

    public OpcodeDiff(int count, int opcode, String arg1, String arg2, String arg3) {
        super(count, opcode, arg1, arg2, arg3);
    }

    public OpcodeDiff(int count, int opcode, int arg) {
        super(count, opcode, arg);
    }

    public OpcodeDiff(int count, int opcode, Label label) {
        super(count, opcode, label);
    }

    public OpcodeDiff(int count, int opcode, Object obj) {
        super(count, opcode, obj);
    }

    public OpcodeDiff(int count, int opcode, Label arg0, int[] arg1, Label[] arg2) {
        super(count, opcode, arg0, arg1, arg2);
    }

    public OpcodeDiff(int count, int opcode, String desc, int dims) {
        super(count, opcode, desc, dims);
    }

    public OpcodeDiff(int count, int opcode, int min, int max, Label dflt, Label... labels) {
        super(count, opcode, min, max, dflt, labels);
    }

    public OpcodeDiff(int count, int opcode, String arg) {
        super(count, opcode, arg);
    }

    public OpcodeDiff(int count, String arg1, String arg2, int arg3, String arg4) {
        super(count, arg1, arg2, arg3, arg4);
    }

    public OpcodeInfo getCurrent(){
        return current;
    }

    //次のOpcodeへとすすむ
    public void change(OpcodeInfo info) {
        current = info;
    }

    //次に割り込む
    public void addNext(OpcodeInfo info) {
        after.add(info);
    }

    //前に割り込む
    public void addBefore(OpcodeInfo info) {
        before.add(info);
    }

    public boolean isTrans() {
        return !(before.size() == 0 && current == null && after.size() == 0) || getOpcode() == -1;
    }

    //今のOpcodeを消す
    public void remove() {
        removeOpcode();
    }
}
