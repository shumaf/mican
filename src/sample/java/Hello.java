import java.io.*;
import java.lang.invoke.*;
import java.lang.invoke.MethodHandles.Lookup;

public class Hello {
	 public static CallSite bsm(Lookup lookup, String name, MethodType methodType) throws Throwable {
		MethodType vmt = methodType.dropParameterTypes(0, 1);
        MethodHandle mh = MethodHandles.lookup().findVirtual(methodType.parameterType(0), "println", vmt);
        return new ConstantCallSite(mh);
	}
	public static void main(String[] args) {
		System.out.println("hello");
		int i = 0;
		i--;
		i = i + 1;
	}
}
